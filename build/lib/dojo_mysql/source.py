from __future__ import unicode_literals

import math
import json
import MySQLdb

from datetime import datetime

from dojo.vanilla.dataset import VanillaDataset


class MySQLSource(VanillaDataset):

    def process(self, inputs):
        conn = MySQLdb.connect(host='localhost',
                               user='testuser',
                               passwd='testpass',
                               db='test')
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute("SELECT name, category FROM animal")
        result_set = cursor.fetchall()
        for row in result_set:
            print "%s, %s" % (row["name"], row["category"])
        print "Number of rows returned: %d" % cursor.rowcount
