from __future__ import unicode_literals

import pytest
import tempfile


@pytest.fixture(scope='function')
def temp_dir():
    with tempfile.TemporaryDirectory() as d:
        yield d
